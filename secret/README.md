# Create the Secret with the 'kubectl create secret' Command

* To create a Secret, we can use the kubectl create secret command:

```
kubectl create secret generic mysql-pass --from-literal=password=mysqlpassword
```

The above command would create a secret called my-password, which has the value of the password key set to mysqlpassword.

# 'get' and 'describe' the Secret

Analyzing the get and describe examples below, we can see that they do not reveal the content of the Secret. The type is listed as Opaque.

```
kubectl get secret my-password
```

# Create a Secret Manually
* We can also create a Secret manually, using the YAML configuration file. With Secrets, each object data must be encoded using base64. If we want to have a configuration file for our Secret, we must first get the base64 encoding for our password:

```
$ echo mysqlpassword | base64

bXlzcWxwYXNzd29yZAo=
```

* and then use it in the configuration file:

```
apiVersion: v1
kind: Secret
metadata:
  name: my-password
type: Opaque
data:
  password: bXlzcWxwYXNzd29yZAo=


```

# Use Secrets Inside Pods

```
.....
    spec:
      containers:
      - image: wordpress:4.7.3-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: wordpress-mysql
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: my-password
              key: password
.....
```
# Volumes
* Create Directory to use as hostPath volume
 
```
mkdir -p /webroot
```

* Create nginx index file

```
echo "<h1>This is my custom nginx index from volume</h1>" > /webroot/index.html
```

* Create Deployment with volumeMounts config

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  selector:
    matchLabels:
      app: nginx
      tier: web
  template:
    metadata:
      labels:
        app: nginx
        tier: web
    spec:
      containers:
      - image: nginx:1.9.1
        name: nginx
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        - name: COMPANY
          valueFrom:
            configMapKeyRef:
              name: customer-ibm
              key: COMPANY

        ports:
        - containerPort: 80
          name: web
        volumeMounts: 
        - mountPath: "/usr/share/nginx/html/"
          name: webroot-volume
      volumes:
      - name: webroot-volume
        hostPath:
          path: "/webroot"
          type: Directory
```
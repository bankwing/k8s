# RSVP Application

![alt text](multitier/rsvp.png "Logo Title Text 1")

# Create the Deployment for MongoDB
* Create an rsvp-db.yaml file with the following content:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rsvp-db
  labels:
    appdb: rsvpdb
spec:
  replicas: 1
  selector:
    matchLabels:
      appdb: rsvpdb
  template:
    metadata:
      labels:
        appdb: rsvpdb
    spec:
      containers:
      - name: rsvp-db
        image: mongo:3.3
        ports:
        - containerPort: 27017
```

* and run the following command to create the rsvp-db Deployment:

```
kubectl create -f rsvp-db.yaml
```

# Create the Service for MongoDB
* To create a mongodb Service for the backend, create an rsvp-db-service.yaml file with the following content:

```
apiVersion: v1
kind: Service
metadata:
  name: mongodb
  labels:
    app: rsvpdb
spec:
  ports:
  - port: 27017
    protocol: TCP
  selector:
    appdb: rsvpdb
```

* and run the following command to create a Service named mongodb to access the backend:

```
kubectl create -f rsvp-db-service.yaml
```
# Create the Deployment for the 'rsvp' Frontend
* To create the rsvp frontend, create an rsvp-web.yaml file, with the following content:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rsvp
  labels:
    app: rsvp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: rsvp
  template:
    metadata:
      labels:
        app: rsvp
    spec:
      containers:
      - name: rsvp-app
        image: teamcloudyuga/rsvpapp
        env:
        - name: MONGODB_HOST
          value: mongodb
        ports:
        - containerPort: 5000
          name: web-port
```

* and run the following command to create the Deployment:

```
kubectl create -f rsvp-web.yaml
```

# Create the Service for the 'rsvp' Frontend
* To create the rsvp Service for our frontend, create an rsvp-web-service.yaml file with the following content:

```
apiVersion: v1
kind: Service
metadata:
  name: rsvp
  labels:
    app: rsvp
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: web-port
    protocol: TCP
    nodePort: 30000
  selector:
    app: rsvp
```

# Scale the Frontend
* Currently, we have one replica running for the frontend. To scale it to 3 replicas, we can use the following command:

```
kubectl scale deployment rsvp --replicas=3
```
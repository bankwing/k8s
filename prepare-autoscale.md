```
vi ~/.kubespray/roles/kubernetes/node/defaults/main.yml
```

kube_read_only_port: 10255

```
kubespray deploy
```

```
netstat -an|grep 10255
```

```
kubectl apply -f https://gitlab.com/bankwing/k8s/raw/master/k8s-dashboard-svc.yml
```

```
git clone https://github.com/kubernetes-incubator/metrics-server.git
cd metrics-server
kubectl create -f deploy/1.8+/
```

